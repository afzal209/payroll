@extends('layout.portal')
@section('title', 'App Setting')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'App Setting')
@section('page_name', 'App Setting')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    @if (Session::has('message') > 0)
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
                    <h6>View Payroll</h6>

                    {{-- @dd('setting-create') --}}

                    <h3 class="card-title">
                        {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                            <i class="fa fa-plus"> Add App Setting</i>
                        </button> --}}
                        @can('setting-create')

                            <a href="/add_page_setting" class="btn btn-info btn-md"><i class="fa fa-plus"> Add App
                                    Setting</i></a>
                        @endcan
                    </h3>


                    <div class="modal fade mt-5" id="addappsetting">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title w-100 text-center">
                                        <i class="fa fa-plus">Add Job Nature </i>
                                    </h4>

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="card card-primary">
                                    <form role="form" id="quickForm" enctype="multipart/form-data">
                                        <input type="hidden" name="get_created_by" id="get_created_by">

                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="fn">Job Nature Service Name:
                                                            <span style="color: red">*</span></label>
                                                        <input class="form-control" type="text" id="fn"
                                                            name="jbn_service_name" placeholder="Job Nature Service Name"
                                                            size="40" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mn">Job Nature Description:
                                                            <span style="color: red">*</span></label>
                                                        <input type="text" class="form-control" value="" name="jbn_desc"
                                                            id="mn" placeholder="Job Nature Description" size="40" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mn">Job Nature Remarks:
                                                            <span style="color: red">*</span></label>
                                                        <input type="text" class="form-control" value=""
                                                            name="jbn_remarks" id="mn" placeholder="Job Nature Remarks"
                                                            size="40" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="checkbox" name="jbn_active" id="jbn_active_hide"
                                                            class="" />
                                                        <input type="hidden" name="itm_active">
                                                        <label for="jbn_active_hide">Active</label>
                                                    </div>
                                                    <div class="modal-footer  justify-content-between">
                                                        <button type="submit" class="btn btn-info">
                                                            <i class="fa fa-check"></i> Submit
                                                        </button>
                                                        <button type="submit" class="btn btn-info">
                                                            <i class="fa fa-spinner"></i> Update
                                                        </button>
                                                        <button class="btn btn-danger btn-md" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <i class="fa fa-times"></i> Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table table-striped table-bordered table-hover" id="table_id">
                            <thead>
                                <tr>
                                    <th>Code </th>
                                    <th>Value</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($app_setting as $app)
                                    <tr>
                                        <td>{{ $app->code }}</td>
                                        <td>{{ $app->value }}</td>
                                        <td>{{ $app->description }}</td>
                                        @can('setting-edit')
                                            <td><a href="/edit_setting/{{ base64_encode($app->id) }}"><i
                                                        class="fa fa-edit"></i></a>
                                            @endcan
                                        </td>
                                    </tr>

                                @endforeach --}}


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuSetting').find('a').addClass('active')
            // alert('yes');
            $('#table_id').DataTable({
                "ajax": {
                    "url": "/app_setting_data",
                    "data": {
                        "user_id": 451
                    }
                }
            });
        });
    </script>
@endsection
