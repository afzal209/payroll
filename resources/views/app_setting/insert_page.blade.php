@extends('layout.portal')
@section('title', 'Add App Setting')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'App Setting')
@section('page_name', 'Add App Setting')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Add App Setting</h6>
                    <h3 class="card-title">
                        {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                            <i class="fa fa-plus"> Add App Setting</i>
                        </button> --}}
                        <a href="/app_setting" class="btn btn-info btn-md"><i class="fa fa-back">Back App Setting</i></a>
                    </h3>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    {{-- @if (Session::has('message') > 0) --}}
                        {{-- <div class="alert alert-success">{{ Session::get('message') }}</div> --}}
                    {{-- @endif --}}
                        <div class="alert alert-success" id="success" style="display: none;"></div>
                        <div class="alert alert-danger" id="error" style="display: none;"></div>

                    <form role="form text-left" action="" method="POST" id="settingform">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Code" aria-label="Code"
                                        aria-describedby="email-addon" name="code" id="code"
                                        style="text-transform: uppercase;">
                                </div>
                                {{-- <div class="mb-3">
                                    <label for="text">Textbox</label>
                                    <input type="radio" placeholder="Code" aria-label="Text" aria-describedby="email-addon"
                                        name="tag" id="tag" value="text">
                                    <label for="image">Chose file</label>
                                    <input type="radio" placeholder="Code" aria-label="Image" aria-describedby="email-addon"
                                        name="tag" id="tag" value="image">
                                </div> --}}
                                <div class="mb-3">
                                    <input type="text" class="form-control text" placeholder="Value" aria-label="Value"
                                        aria-describedby="email-addon" name="value" id="text">
                                </div>
                                {{-- <div class="mb-3">
                                    <input type="file" class="form-control image" placeholder="Value" aria-label="Value"
                                        aria-describedby="email-addon" name="image" id="image" style="display: none">
                                </div> --}}
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Description"
                                        aria-label="Description" aria-describedby="email-addon" name="description"
                                        id="description">
                                </div>


                                {{-- <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        aria-label="Password" aria-describedby="password-addon">
                                </div> --}}
                                {{-- <div class="form-check form-check-info text-left">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                            Conditions</a>
                                    </label>
                                </div> --}}
                                <div class="text-center">
                                    {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuSetting').find('a').addClass('active')
            // alert('yes');
            $('input[type=radio][name=tag]').on('change', function() {
                // alert('work');
                // console.log(this.value);
                if (this.value == 'text') {
                    $("#text").show();
                    $('#image').hide();
                } else {
                    $('#text').hide();
                    $('#image').show();
                }
            })
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#settingform').on('submit',function(e){
                e.preventDefault();
                // alert('yes');

                var form = new FormData(this);
                $.ajax({
                    url: '/insert_setting',
                    type: 'POST',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        //    console.log(data);
                        $(data).each(function(key, value) {
                            if (value.success) {
                                $('#error').html('');
                                $('#error').hide();
                                $('#success').show();
                                $('#success').append(value.success);
                                $('#settingform').trigger('reset');
                                $('#settingform').scrollTop($('#success'));
                            } else {

                                $('#success').hide();
                                $('#error').show();
                                $('#error').html('');
                                $('#success').html('');
                                var error = data.error.toString().replaceAll(',',
                                    '<br/>');
                                $('#error').html(error);
                                $('#settingform').scrollTop($('#error'));
                            }
                        })
                        // console.log('yes');


                    }
                })
            })
        });
    </script>
@endsection
