@extends('layout.portal')
@section('title', 'Update App Setting')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'App Setting')
@section('page_name', 'Update App Setting')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Add App Setting</h6>
                    <h3 class="card-title">
                        {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                            <i class="fa fa-plus"> Add App Setting</i>
                        </button> --}}
                        <a href="/app_setting" class="btn btn-info btn-md"><i class="fa fa-back">Back App Setting</i></a>
                    </h3>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    @if (Session::has('message') > 0)
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
                    {{-- <form role="form text-left" action="{{ url('/update_setting', $app_setting->id) }}" method="POST"> --}}
                    <form role="form text-left" action="" method="POST" id="updatesetting">
                        <input type="hidden" name="id" value="{{ base64_encode($app_setting->id.'|i') }}" id="id">

                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Code" aria-label="Code"
                                        aria-describedby="email-addon" name="code" id="code"
                                        value="{{ $app_setting->code }}" readonly>
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Value" aria-label="Value"
                                        aria-describedby="email-addon" name="value" id="value"
                                        value="{{ $app_setting->value }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Description"
                                        aria-label="Description" aria-describedby="email-addon" name="description"
                                        id="description" value="{{ $app_setting->description }}">
                                </div>


                                {{-- <div class="mb-3">
                                        <input type="password" class="form-control" placeholder="Password"
                                            aria-label="Password" aria-describedby="password-addon">
                                    </div> --}}
                                {{-- <div class="form-check form-check-info text-left">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                                Conditions</a>
                                        </label>
                                    </div> --}}
                                <div class="text-center">
                                    {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            // alert('yes');
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuSetting').find('a').addClass('active');

            $('#updatesetting').on('submit',function(e){
                e.preventDefault();
                var form = new FormData(this);
                $.ajax({
                    url : "{{ url('/update_setting'). '/euyf' }}",
                    type : "POST",
                    data :  form,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success : function(data){
                        console.log(data);
                        $(data).each(function(key,value){
                            if (value.success) {

                                // window.location.href = "/view";
                                history.back()
                                // console.log(value.success);
                                alert(value.success)
                            }

                        });
                    }

                })
            })
        });

    </script>
@endsection
