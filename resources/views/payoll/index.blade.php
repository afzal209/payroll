    @extends('layout.portal')
    @section('title', 'Add Payroll')
    @section('breadcrumb_main', 'Dashboard')
    @section('breadcrumb_active', 'Payroll')
    @section('page_name', 'Add Payroll')
    @section('header-style')
        <style>
            .ui-datepicker-calendar {
                display: none;
            }

        </style>
    @endsection
    @section('content')
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    <div class="card-header pb-0">
                        <h6>Add Payroll</h6>
                    </div>
                    {{-- <div class="card-body px-0 pt-0 pb-2">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif --}}


                    {{-- @if (Session::has('message') > 0) --}}
                    <div class="alert alert-success" id="success" style="display: none"></div>
                    <div class="alert alert-danger" id="error" style="display: none"></div>
                    {{-- @endif --}}
                    <form role="form text-left" action="" method="POST" id="payform">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <select name="user_id" id="user_id" class="form-control">
                                        <option value="">Select User</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Date of Brith" aria-label="Name"
                                        aria-describedby="email-addon" name="month_year" id="month_year">
                                </div>
                                <div class="mb-3">
                                    <input type="number" class="form-control" placeholder="Salary" aria-label="Name"
                                        aria-describedby="email-addon" name="salary" id="salary">
                                </div>



                                {{-- <div class="mb-3">
                                        <input type="password" class="form-control" placeholder="Password"
                                            aria-label="Password" aria-describedby="password-addon">
                                    </div> --}}
                                {{-- <div class="form-check form-check-info text-left">
                                        <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                        <label class="form-check-label" for="flexCheckDefault">
                                            I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                                Conditions</a>
                                        </label>
                                    </div> --}}
                                <div class="text-center">
                                    {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p>
                            </div>
                        </div>


                    </form>

                </div>
            </div>
        </div>
        </div>
    @endsection
    @section('footer_script')
        <script>
            $(document).ready(function() {
                $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
                $('#liMenuPayRol').find('a').addClass('active')
                // alert('yes');
                $('#month_year').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'MM yy',

                    onClose: function() {
                        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                    },

                    beforeShow: function() {
                        if ((selDate = $(this).val()).length > 0) {
                            iYear = selDate.substring(selDate.length - 4, selDate.length);
                            iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5),
                                $(this).datepicker('option', 'monthNames'));
                            $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
                            $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
                        }
                    }
                });
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#payform').on('submit', function(e) {
                    e.preventDefault();
                    // alert('yes');
                    var form = new FormData(this);
                    $.ajax({
                        url: "/insert_payroll",
                        type: "POST",
                        data: form,
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            // console.log(data);
                            $(data).each(function(key, value) {
                                if (value.success) {
                                    $('#error').html('');
                                    $('#error').hide();
                                    $('#success').show();
                                    $('#success').append(value.success);
                                    $('#payform').trigger('reset');
                                    $('#payform').scrollTop($("#success"));
                                } else {

                                    $('#success').hide();
                                    $('#error').show();
                                    $('#error').html('');
                                    $('#success').html('');
                                    var error = data.error.toString().replaceAll(',',
                                        '<br/>');
                                    $('#error').html(error);
                                    $('#payform').scrollTop($("#error"));
                                }
                            })
                        }
                    })
                })


            });
        </script>
    @endsection
