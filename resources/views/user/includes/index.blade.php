    <div class="card mb-4">
        <div class="card-header pb-0">
            <h6>Add User</h6>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            {{-- @if (Session::has('add_message') > 0) --}}
                <div class="alert alert-success" id="success" style="display: none"></div>
            {{-- @elseif (Session::has('error') > 0) --}}
                <div class="alert alert-danger" id="error" style="display: none"></div>
            {{-- @endif --}}

            @include('user.includes.adminFrom')

        </div>
    </div>

    <div class="card mb-4">
        <div class="card-header pb-0">
            <h6>Add User Detail</h6>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            {{-- @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif --}}
            {{-- @if (Session::has('message') > 0) --}}
            <div class="alert alert-success" id="success_detail" style="display: none"></div>
            <div class="alert alert-danger" id="error_detail" style="display: none"></div>
            {{-- @endif --}}

            @include('user.includes.userForm')

        </div>
    </div>
