<form role="form text-left" action="/user_detail" method="POST" id="userform">
    @csrf
    <div class="container">
        <div class="row">
            <div class="mb-3">
                <select name="user_id" id="user_id" class="form-control">
                    <option value="">Select User</option>
                    @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="First Name" aria-label="Name"
                    aria-describedby="email-addon" name="first_name" id="first_name">
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Last Name" aria-label="Name"
                    aria-describedby="email-addon" name="last_name" id="last_name">
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Date of Brith" aria-label="Name"
                    aria-describedby="email-addon" name="date_of_brith" onfocus="(this.type='date')"
                    onblur="(this.type='text')" id="date_of_brith">
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Date of Joining" aria-label="Name"
                    aria-describedby="email-addon" name="date_of_joining" onfocus="(this.type='date')"
                    onblur="(this.type='text')" id="date_of_joining">
            </div>
            <div class="mb-3">
                <input type="number" class="form-control" placeholder="National Identity Card" aria-label="Name"
                    aria-describedby="email-addon" name="nic" id="nic">
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Next of Kin" aria-label="Name"
                    aria-describedby="email-addon" name="next_of_kin" id="next_of_kin">
            </div>
            <div class="mb-3">
                <input type="number" class="form-control" placeholder="Phone Number" aria-label="Name"
                    aria-describedby="email-addon" name="phone_number" id="phone_number">
            </div>

            {{-- <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        aria-label="Password" aria-describedby="password-addon">
                                </div> --}}
            {{-- <div class="form-check form-check-info text-left">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                            Conditions</a>
                                    </label>
                                </div> --}}
            <div class="text-center">
                {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
            </div>
            <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                    class="text-dark font-weight-bolder">Sign in</a></p>
        </div>
    </div>


</form>
