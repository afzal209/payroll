<form role="form text-left" action="" method="POST" id="adminform">
    @csrf
    <div class="container">
        <div class="row">
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Username" aria-label="Name"
                    aria-describedby="user-addon" name="name" id="name">
                <h6 id="space" style="display: none"></h6>
            </div>

            <div class="mb-3">
                <input type="email" class="form-control" placeholder="Email" aria-label="Email"
                    aria-describedby="email-addon" name="email">
            </div>
            <div class="mb-3">
                <input type="password" class="form-control" placeholder="Password" aria-label="Password"
                    aria-describedby="password-addon" name="password" id="password">
                <h6 id="passlen" style="display: none;"></h6>
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" placeholder="Employee Id" aria-label="Name"
                    aria-describedby="user-addon" name="emp_id">
            </div>
            <div class="mb-3">
                <select name="role_id[]" id="role_id" class="form-control">
                    <option value="">Select Role</option>
                    @foreach ($user_role as $user_r)
                        <option value="{{ $user_r->id }}">{{ $user_r->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="text-center">
                <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
            </div>
            <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                    class="text-dark font-weight-bolder">Create</a></p>
        </div>
    </div>


</form>
