@extends('layout.portal')
@section('title', 'View User')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'View User')
@section('page_name', 'View User')

@section('content')
    {{-- <div class="container-fluid py-4"> --}}
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                {{-- @if (Session::has('message') > 0) --}}
                <div class="alert alert-danger" id="success" style="display: none;"></div>
                {{-- @endif --}}
                <div class="card-header pb-0">
                    <h6>View User</h6>
                    @can('user-create')
                        <h3 class="card-title">
                            {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                            <i class="fa fa-plus"> Add App Setting</i>
                        </button> --}}
                            <a href="/user" class="btn btn-info btn-md"><i class="fa fa-plus"> Add User</i></a>
                            <button onclick='reloads()' class="btn btn-info btn-md">reload</button>
                        </h3>
                    @endcan

                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table table-striped table-bordered table-hover" id="table_id">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- @foreach ($user as $userData)
                                    <tr>
                                        <td>{{ $userData->Username }}</td>
                                        <td>{{ $userData->first_name }}</td>
                                        <td>{{ $userData->last_name }}</td>
                                        <td>{{ $userData->email }}</td>
                                        <td>
                                            @can('user-edit')
                                                <a href="/edit/{{ $userData->id }}"><i class="fas fa-edit"></i></a>/
                                            @endcan
                                            @can('user-delete')
                                                <form action="{{ url('/delete', $userData->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button><i class="fas fa-trash"></i></button>
                                                </form>/
                                            @endcan
                                            @can('user-permission')
                                                <a href="/more_permission/{{ $userData->id }}"><i
                                                        class="fa fa-lock"></i></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach --}}


                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
@endsection
@section('footer_script')
    <script>
        $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
        $('#liMenuView').find('a').addClass('active')
        $(document).ready(function() {
            $('#table_id').DataTable({
                "ajax": {
                    "url": "/view_data",
                    "data": {
                        "user_id": 451
                    }
                }
            });


        });

        function reloads() {
            $('#success').html('');
            $('#success').hide();
            $('#table_id').DataTable().ajax.reload();

        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function delete_user(value) {
            // console.log(value);
            var id = $(value).attr('data-id');
            // console.log(id);
            $.ajax({
                url: "{{ url('/delete') . '/' }}" + id,
                type: "DELETE",
                data: id,
                success: function(data) {
                    // console.log(data);
                    $(data).each(function(key, value) {
                        $('#success').show();
                        $('#success').html('');
                        $('#success').append(value.success)
                    })


                }
            })
        }
    </script>



@endsection
