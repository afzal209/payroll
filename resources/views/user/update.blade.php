@extends('layout.portal')
@section('title', 'Add User')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'Update User')
@section('page_name', 'Update User')

@section('content')
    {{-- <div class="container-fluid py-4"> --}}
    <div class="row">
        <h3 class="card-title">
            {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                <i class="fa fa-plus"> Add App Setting</i>
            </button> --}}
            <a href="/view" class="btn btn-info btn-md"><i class="fa fa-plus"> View User</i></a>

        </h3>
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Update User</h6>
                </div>
                <div class="alert alert-danger" id="error" style="display: none"></div>
                <div class="card-body px-0 pt-0 pb-2">
                    {{-- <form role="form text-left" action="{{ url('/update', $user->id) }}" method="POST"> --}}
                        <form role="form text-left" action="" method="POST" id="updateadminform">
                            <input type="hidden" name="id" value="{{ base64_encode($user->id.'|i') }}" id="id">

                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Username" aria-label="Name"
                                        aria-describedby="user-addon" name="name" id="name" value="{{ $user->name }}">
                                    <h6 id="space" style="display: none"></h6>
                                </div>
                                {{-- <div class="mb-3">
                                    <input type="email" class="form-control" placeholder="Email" aria-label="Email"
                                        aria-describedby="email-addon" name="email" value="{{ $user->email }}">
                                </div> --}}
                                <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        aria-label="Password" aria-describedby="password-addon" name="password">
                                </div>
                                <div class="mb-3">
                                    <select name="role_id" id="role_id" class="form-control">
                                        <option value="">Select Role</option>
                                        @foreach ($roles as $role)
                                            {{ $selected = '' }}
                                            @if (in_array($role, $userRole))
                                                @php
                                                    $selected = 'selected';
                                                @endphp
                                            @endif
                                            <option {{ $selected }} value="{{ $role }}">
                                                {{ $role }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        aria-label="Password" aria-describedby="password-addon">
                                </div> --}}
                                {{-- <div class="form-check form-check-info text-left">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                            Conditions</a>
                                    </label>
                                </div> --}}
                                <div class="text-center">
                                    {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                {{-- <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-header pb-0">
                    <h6>Update User Detail</h6>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    {{-- <form role="form text-left" action="{{ url('/updatedetail', $user->id) }}" method="POST"> --}}
                    <form role="form text-left" action="" method="POST" id="updateuserform">
                        <input type="hidden" name="id_detail" value="{{ base64_encode($user->id.'|i') }}" id="id_detail">

                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <input type="hidden" class="form-control" placeholder="Employee Id" aria-label="Name"
                                        aria-describedby="user-addon" name="emp_id" value="{{ $user_detail->emp_id }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="First Name" aria-label="Name"
                                        aria-describedby="email-addon" name="first_name"
                                        value="{{ $user_detail->first_name }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Last Name" aria-label="Name"
                                        aria-describedby="email-addon" name="last_name"
                                        value="{{ $user_detail->last_name }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Date of Brith" aria-label="Name"
                                        aria-describedby="email-addon" name="date_of_brith" onfocus="(this.type='date')"
                                        onblur="(this.type='text')" value="{{ $user_detail->date_of_brith }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Date of Joining"
                                        aria-label="Name" aria-describedby="email-addon" name="date_of_joining"
                                        onfocus="(this.type='date')" onblur="(this.type='text')"
                                        value="{{ $user_detail->date_of_joining }}">
                                </div>
                                <div class="mb-3">
                                    <input type="number" class="form-control" placeholder="National Identity Card"
                                        aria-label="Name" aria-describedby="email-addon" name="nic"
                                        value="{{ $user_detail->nic }}">
                                </div>
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Next of Kin" aria-label="Name"
                                        aria-describedby="email-addon" name="next_of_kin"
                                        value="{{ $user_detail->next_of_kin }}">
                                </div>
                                <div class="mb-3">
                                    <input type="number" class="form-control" placeholder="Phone Number" aria-label="Name"
                                        aria-describedby="email-addon" name="phone_number"
                                        value="{{ $user_detail->phone_number }}">
                                </div>

                                <div class="text-center">
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                {{-- <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p> --}}
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- </div> --}}
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuView').find('a').addClass('active');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#updateadminform').on('submit',function(e){
                e.preventDefault();
                // var form = new FormData(this);
                var form = new FormData(this);
                // console.log(id);
                $.ajax({
                    url : "{{ url('/update'). '/euyf' }}",
                    type : "POST",
                    data :  form,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success : function(data){
                        // console.log(data);
                        $(data).each(function(key,value){
                            if (value.success) {
                                $('#error').hide();
                                $('#error').html('');
                                // window.location.href = "/view";
                                history.back()
                                // console.log(value.success);
                                alert(value.success)
                            }
                            else{
                                $('#error').show();
                                $('#error').html('');
                                var error = data.error.toString().replaceAll(',',
                                    '<br/>');
                                $('#error').html(error);
                                $('#updateadminform').scrollTop($("#error"));

                            }
                        });
                    }

                })

            })
            $('#updateuserform').on('submit',function(e){
                e.preventDefault();
                // alert('yes');
                var form = new FormData(this);
                // console.log(form);
                $.ajax({
                    url : "{{ url('/updatedetail'). '/edf' }}",
                    type : "POST",
                    data :  form,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success : function(data){
                        // console.log(data);
                        $(data).each(function(key,value){
                            if (value.success) {
                                history.back();
                                // console.log(value.success);
                                alert(value.success)
                            }

                        });
                    }
                })

            })
        })
    </script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
@endsection
