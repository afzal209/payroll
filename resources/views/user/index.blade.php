@extends('layout.portal')
@section('title', 'Add User')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'Add User')
@section('page_name', 'Add User')

@section('content')
    {{-- <div class="container-fluid py-4"> --}}
    <div class="row">
        <h3 class="card-title">
            {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                <i class="fa fa-plus"> Add App Setting</i>
            </button> --}}
            <a href="/view" class="btn btn-info btn-md"><i class="fa fa-plus"> View User</i></a>

        </h3>
        <div class="col-12">
            @include('user.includes.index')
        </div>
    </div>
    {{-- </div> --}}
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuView').find('a').addClass('active')
            // var name = $('#name').val();
            // console.log(name);
            // alert('yes');
            $('#name').keyup(function() {
                var name = $(this).val();
                $('#space').text('');
                // console.log(name);
                if (name.indexOf(' ') > 0) {
                    $('#space').show();
                    $('#space').append('Username Cannot have Space');
                    // alert('Username Cannot have space')
                } else {
                    $('#space').hide();
                }
            })
            $('#password').on('change', function() {
                // alert('Yes')
                var password = $(this).val();
                $('#passlen').text('');

                if (password.length < 8) {
                    // alert('Yes')
                    $('#passlen').show();
                    $('#passlen').append('Password length must be 8 charater');
                } else {
                    $('#passlen').hide();
                }
            })
            $('#user_id').on('change', function() {
                // alert('yes');
                var user_id = $(this).val();
                // console.log(user_id);
                if (user_id === '') {
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#date_of_brith').val('');
                    $('#date_of_joining').val('');
                    $('#nic').val('');
                    $('#next_of_kin').val('');
                    $('#phone_number').val('');
                } else {
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#date_of_brith').val('');
                    $('#date_of_joining').val('');
                    $('#nic').val('');
                    $('#next_of_kin').val('');
                    $('#phone_number').val('');
                    // console.log(user_id);
                    $.ajax({
                        type: "GET",
                        url: "{{ url('/getUserdt') . '/' }}" + user_id,
                        success: function(data) {
                            // console.log(data);
                            $(data).each(function(key, value) {
                                // console.log(value);
                                $('#first_name').val(value.first_name);
                                $('#last_name').val(value.last_name);
                                $('#date_of_brith').val(value.date_of_brith);
                                $('#date_of_joining').val(value.date_of_joining);
                                $('#nic').val(value.nic);
                                $('#next_of_kin').val(value.next_of_kin);
                                $('#phone_number').val(value.phone_number);
                            });

                        }

                    })
                }

            })
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#adminform').on('submit', function(e) {
                e.preventDefault();
                // console.log('working');

                var form = new FormData(this);
                $.ajax({
                    url: '/insert',
                    type: 'POST',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        //    console.log(data);
                        $(data).each(function(key, value) {
                            if (value.success) {
                                $('#error').html('');
                                $('#error').hide();
                                $('#success').show();
                                $('#success').append(value.success);
                                $('#adminform').trigger('reset');
                                $('#adminform').scrollTop($("#success"));
                            } else {

                                $('#success').hide();
                                $('#error').show();
                                $('#error').html('');
                                $('#success').html('');
                                var error = data.error.toString().replaceAll(',',
                                    '<br/>');
                                $('#error').html(error);
                                $('#adminform').scrollTop($("#error"));
                            }
                        })
                        // console.log('yes');


                    }
                })

            })

            $('#userform').on('submit', function(e) {
                e.preventDefault();
                // console.log('working');
                // $('#success_detail').html('');
                var form = new FormData(this);
                $.ajax({
                    url: '/user_detail',
                    type: 'POST',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        //    console.log(data);
                        $(data).each(function(key, value) {
                            if (value.success) {
                                $('#error_detail').html('');
                                $('#error_detail').hide();
                                $('#success_detail').show();
                                $('#success_detail').append(value.success);
                                $('#userforms').trigger('reset');
                                $('#userform').scrollTop($("#success_detail"));
                            } else {

                                $('#success_detail').hide();
                                $('#error_detail').show();
                                $('#error_detail').html('');
                                $('#success_detail').html('');
                                var error = data.error.toString().replaceAll(',',
                                    '<br/>');
                                $('#error_detail').html(error);
                                $('#userform').scrollTop($("#error_detail"));

                            }
                            // $('#success_detail').show();
                            // $('#success_detail').append(value.success);
                            // $('#userforms').trigger('reset');

                        })
                        // console.log('yes');


                    }
                })

            })
        })
    </script>
    <script>
        var win = navigator.platform.indexOf('Win') > -1;
        if (win && document.querySelector('#sidenav-scrollbar')) {
            var options = {
                damping: '0.5'
            }
            Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
        }
    </script>
@endsection
