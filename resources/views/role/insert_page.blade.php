@extends('layout.portal')
@section('title', 'Add Role')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'Add Role')
@section('page_name', 'Add Role')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-header pb-0">
                    @if (Session::has('message') > 0)
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif
                    <h6>Add Role</h6>

                    {{-- @dd('setting-create') --}}

                    <h3 class="card-title">
                        {{-- <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#addappsetting">
                            <i class="fa fa-plus"> Add App Setting</i>
                        </button> --}}
                        {{-- @can('setting-create') --}}

                        <a href="/role" class="btn btn-info btn-md"><i class="fa fa-eye"> View Role</i></a>
                        {{-- @endcan --}}
                    </h3>


                    {{-- <div class="modal fade mt-5" id="addappsetting">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title w-100 text-center">
                                        <i class="fa fa-plus">Add Job Nature </i>
                                    </h4>

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="card card-primary">
                                    <form role="form" id="quickForm" enctype="multipart/form-data">
                                        <input type="hidden" name="get_created_by" id="get_created_by">

                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="fn">Job Nature Service Name:
                                                            <span style="color: red">*</span></label>
                                                        <input class="form-control" type="text" id="fn"
                                                            name="jbn_service_name" placeholder="Job Nature Service Name"
                                                            size="40" value="" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mn">Job Nature Description:
                                                            <span style="color: red">*</span></label>
                                                        <input type="text" class="form-control" value="" name="jbn_desc"
                                                            id="mn" placeholder="Job Nature Description" size="40" />
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="mn">Job Nature Remarks:
                                                            <span style="color: red">*</span></label>
                                                        <input type="text" class="form-control" value=""
                                                            name="jbn_remarks" id="mn" placeholder="Job Nature Remarks"
                                                            size="40" />
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="checkbox" name="jbn_active" id="jbn_active_hide"
                                                            class="" />
                                                        <input type="hidden" name="itm_active">
                                                        <label for="jbn_active_hide">Active</label>
                                                    </div>
                                                    <div class="modal-footer  justify-content-between">
                                                        <button type="submit" class="btn btn-info">
                                                            <i class="fa fa-check"></i> Submit
                                                        </button>
                                                        <button type="submit" class="btn btn-info">
                                                            <i class="fa fa-spinner"></i> Update
                                                        </button>
                                                        <button class="btn btn-danger btn-md" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <i class="fa fa-times"></i> Cancel
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif


                    {{-- @if (Session::has('message') > 0)
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @endif --}}
                    <div class="alert alert-success" id="success" style="display: none;"></div>
                    <div class="alert alert-danger" id="error" style="display: none;"></div>

                    <form role="form text-left" action="" method="POST" id="roleform" id="roleform">
                        @csrf
                        <div class="container">
                            <div class="row">
                                <div class="mb-3">
                                    <input type="text" class="form-control" placeholder="Name" aria-label="Name"
                                        aria-describedby="email-addon" name="name" id="name" value="">
                                </div>
                                <div class="mb-3">
                                    @foreach ($permission as $value)
                                        <div class="form-check form-check-info text-left">


                                            <input class="form-check-input" type="checkbox" value="{{ $value->id }}"
                                                name="permission[]" id="{{ $value->id }}">
                                            <label class="form-check-label" for="{{ $value->id }}">
                                                {{-- I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and Conditions</a> --}}
                                                {{ $value->name }}
                                            </label>


                                        </div>
                                    @endforeach
                                </div>


                                {{-- <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        aria-label="Password" aria-describedby="password-addon">
                                </div> --}}
                                {{-- <div class="form-check form-check-info text-left">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" checked>
                                    <label class="form-check-label" for="flexCheckDefault">
                                        I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and
                                            Conditions</a>
                                    </label>
                                </div> --}}
                                <div class="text-center">
                                    {{-- <button type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button> --}}
                                    <input type="submit" value="Sign up" class="btn bg-gradient-dark w-100 my-4 mb-2">
                                </div>
                                <p class="text-sm mt-3 mb-0">Already have an account? <a href="javascript:;"
                                        class="text-dark font-weight-bolder">Sign in</a></p>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_script')
    <script>
        $(document).ready(function() {
            // alert('yes');
            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuRole').find('a').addClass('active')

            $('#roleform').on('submit',function(e){
                e.preventDefault();
                // alert('yes');
                var form = new FormData(this);
                $.ajax({
                    url: '/insert_role',
                    type: 'POST',
                    data: form,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        //    console.log(data);
                        $(data).each(function(key, value) {
                            if (value.success) {
                                $('#error').html('');
                                $('#error').hide();
                                $('#success').show();
                                $('#success').append(value.success);
                                $('#roleform').trigger('reset');
                                $('#roleform').scrollTop($("#success"));

                            } else {

                                $('#success').hide();
                                $('#error').show();
                                $('#error').html('');
                                $('#success').html('');
                                var error = data.error.toString().replaceAll(',',
                                    '<br/>');
                                $('#error').html(error);
                                $('#roleform').scrollTop($("#error"));

                            }
                        })
                        // console.log('yes');


                    }
                })
            })

        });
    </script>
@endsection
