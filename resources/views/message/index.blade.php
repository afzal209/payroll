@extends('layout.portal')
@section('title', 'Chat Room')
@section('breadcrumb_main', 'Dashboard')
@section('breadcrumb_active', 'Chat Room')
@section('page_name', 'Chat Room')
@section('header-style')

    <style>
        .chat-row {
            margin: 50px;
        }

        .card-body ul {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .card-body ul li {
            padding: 8px;
            background: #928787;
            margin-bottom: 20px;
            color: white
        }

        .card-body ul li:nth-child(2n-2) {
            background: #c3c5c5;
        }

        .chat-input {
            border: 1px soild lightgray;
            border-top-right-radius: 10px;
            border-top-left-radius: 10px;
            padding: 8px 10px;
            color: #fff;
        }

        .draggable {
            height: auto;
            margin: 0;
            padding: 0;
        }

        .move_chat {
            width: 100%;
            padding: 0px 10px;
            background: black;
        }

        #chatInput {
            border: none;
            background-color: transparent;
            resize: none;
            outline: none;
            width: 100%;
        }

        .scroller {
            width: 300px;
            height: 100px;
            overflow-y: scroll;
            scrollbar-width: thin;
        }

        .show_check_name>li {
            margin: 0px 6px;
        }

        .show_check_name {
            display: inherit;
            list-style-type: none;
        }

        .dot {
            height: 35px;
            width: 35px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            text-align: center;

        }

        .char {
            overflow: auto;
            height: 10pc;
        }

        .test {
            height: 35px;
            width: 35px;
            background-color: #bbb;
        }

        [data-title] {
            /* outline: red dotted 1px; */
            /*optional styling*/
            /* margin-top: 100px; */
            /*optional styling*/
            /* font-size: 30px; */
            /*optional styling*/

            position: relative;
            cursor: pointer;
        }

        [data-title]:hover::before {
            content: attr(data-title);
            position: absolute;
            top: -26px;
            display: inline-block;
            padding: 3px 6px;
            border-radius: 2px;
            background: #000;
            color: #fff;
            font-size: 12px;
            font-family: sans-serif;
            white-space: nowrap;
        }

        [data-title]:hover::after {
            content: '';
            position: absolute;
            top: -10px;
            left: 8px;
            display: inline-block;
            color: #fff;
            border: 8px solid transparent;
            border-top: 8px solid #000;
        }

    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="#" style="float: right" onclick="showModal('search')" data-toggle="modal"
                data-target="#search_model"><i class="fa fa-search"></i></a>
        </div>
        <div class="modal fade mt-5" id="search_model" tabindex="-1" role="dialog" aria-labelledby="messageTitle"
            aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h4 class="modal-title w-100 text-center ">
                            <i class="fa fa-search">Search</i>
                        </h4>

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                            onclick="closeSearch()">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card card-primary">
                        <div class="alert alert-success" id="success" style="display: none"></div>
                        <div class="alert alert-danger" id="error" style="display: none"></div>
                        <form role="form" enctype="" id="messageChat" autocomplete="off">
                            <input type="hidden" name="chat_hidden" id="chat_hidden" value="">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-text text-body"><i class="fas fa-search"
                                                    aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" placeholder="Type Username...">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-text text-body"><i class="fas fa-search"
                                                    aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" placeholder="Type Group...">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-4 scroll_main" id="content">

                <div class="move_chat"></div>



                <div class="card-header pb-0">
                    <h6>Message</h6>
                    <h3 class="card-title">
                        <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#message"
                            onclick="showModal('add')" style="float: right">
                            <i class="fa fa-plus"> Add Chat Room</i>
                        </button>
                        <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#message"
                            style="float: right">
                            <i class="fa fa-sort">Sort</i>
                        </button>
                    </h3>
                    <div class="modal fade mt-5" id="message_model" tabindex="-1" role="dialog"
                        aria-labelledby="messageTitle" aria-hidden="true">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title w-100 text-center chat_label">

                                    </h4>

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                                        onclick="closeModal()">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="card card-primary">
                                    <div class="alert alert-success" id="success" style="display: none"></div>
                                    <div class="alert alert-danger" id="error" style="display: none"></div>
                                    <form role="form" enctype="" id="messageChat" autocomplete="off">
                                        <input type="hidden" name="chat_hidden" id="chat_hidden" value="">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <input type="text" name="search" id="search" placeholder="Search Name"
                                                        class="form-control search">
                                                </div>

                                                <div class="col-md-12 show_check_name">

                                                </div>


                                                <div style="display: none " id="scroll_data_store"></div>

                                                <div class=" col-md-12 form-group scroller" id="scroll_data"
                                                    style="width: 100%;"></div>

                                                <div class="form-group name_div">
                                                    <label for="name">Name:
                                                        <span style="color: red">*</span></label>
                                                    <input type="text" class="form-control" value="" name="name" id="name"
                                                        placeholder="Name Of Group" size="40" />
                                                </div>
                                                <div class="form-group icon_div">
                                                    <label for="icon">Icon:
                                                        <span style="color: red">*</span>
                                                    </label>
                                                    <input type="text" class="form-control" value="" name="icon" id="icon"
                                                        placeholder="Icon" size="40" />
                                                </div>

                                                <div class="modal-footer  justify-content-between">
                                                    <button type="submit" id="quickForm" class="btn btn-info">
                                                        <i class="fa fa-check"></i> Submit
                                                    </button>
                                                    <button type="submit" id="adduser" class="btn btn-info"
                                                        style="display: none;">
                                                        <i class="fa fa-user"></i> Add User
                                                    </button>
                                                    <button type="submit" id="searchuser" class="btn btn-info"
                                                        style="display: none;">
                                                        <i class="fa fa-search"></i> Search User
                                                    </button>
                                                    <button class="btn btn-danger btn-md" data-dismiss="modal"
                                                        aria-label="Close" onclick="closeModal()">
                                                        <i class="fa fa-times"></i> Cancel
                                                    </button>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="card-footer"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- <div class="container spark-screen">

                    <div class="row chat-row">
                        <div class="chat-content" id='chat-1'>
                            <ul>

                            </ul>
                        </div>

                        <div class="chat-section">
                            <div class="chat-box row">
                                <input class="col-md-12 chat-input bg-primary" id="chatInput" contenteditable="" />

                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>

@endsection
@section('footer_script')
    <script>
        // var store = 'empt'
        let socket = '';
        $(document).ready(function() {

            $('#sidenav-collapse-main').find('ul').find('li').find('a').removeClass('active');
            $('#liMenuMessage').find('a').addClass('active')
            let ip_address = '127.0.0.1';
            let socket_port = '3000';
            socket = io(ip_address + ':' + socket_port);

            socket.on('sendChatToClient', (message) => {
                // console.log(message)

                append_chat(message[0], message[1], message[2], message[3])

            });



        })

        function key_press(data, text_val, chat_id, sender) {
            // console.log(data.which);

            var type = 'no'

            if ($('#' + text_val).val() != '') {
                type = 'type'
            }

            if (data.which == 13) {
                send(text_val, chat_id, sender, type);
            }
            data = Array(text_val, chat_id, sender, type)
            socket.emit('sendChatToServer', data);
        }

        // function showSearch() {
        //     $('#search_model').modal('show');
        // }

        // function closeSearch() {
        //     $('#search_model').modal('hide');
        // }

        function showModal(val) {
            if (val == 'add') {
                $('#message_model').modal('show');
                $('.show_check_name').html('');

                $('.name_div').show();
                $('.icon_div').show();
                $('input[type=checkbox]').removeAttr('checked')
                $("input[name='user_id[]']").removeClass('this')
                $('.chat_label').append('<i class="fa fa-plus">Create Chat Room </i>');
                $('#adduser').hide();
                $('#quickForm').show();
                $('#searchuser').hide();
            } else if (val == 'edit') {
                $('#message_model').modal('show');
                $('.name_div').hide();
                $('.icon_div').hide();
                $('#name').val('');
                $('#icon').val('');
                // console.log(val);
                $('.chat_label').html('');
                $('.chat_label').append('<i class="fa fa-user">Add User </i>');
            } else if (val == 'search') {
                $('#message_model').modal('show');
                $('#error').hide();
                $('#error').html('');
                $('#success').hide();
                $('#success').html('');
                $('.show_check_name > li').remove();
                $('input[type=checkbox]').removeAttr('checked')
                $('#name').val('');
                $('#icon').val('');
                $('.name_div').hide();
                $('.icon_div').hide();
                $('#name').val('');
                $('#icon').val('');
                $('.chat_label').html('');
                $('.chat_label').append('<i class="fa fa-search">Search</i>');
                $('#adduser').hide();
                $('#quickForm').hide();
                $('#searchuser').show();
            }

        }

        function closeModal() {
            $('#messageChat').trigger('reset');

            $('#message_model').modal('hide');
            $('#error').hide();
            $('#error').html('');
            $('#success').hide();
            $('#success').html('');
            $('.show_check_name > li').remove();
            $('input[type=checkbox]').removeAttr('checked')
            $("input[name='user_id[]']").removeClass('this')
            $('#name').val('');
            $('#icon').val('');
            $('.chat_label').html('');


        }
        $(function() {

            $.ajax({
                url: "/move_chat",
                type: "GET",
                processData: false,
                contentType: false,
                success: function(data) {
                    // console.log(data);
                    let chatHtml = '';
                    chatHtml += `
    <div class="row ">`;
                    $(data.data).each(function(key, value) {

                        // console.log(value);
                        // var getLabel = $('.draggable header').find('div').find('h5').html();
                        // console.log(getLabel);
                        //         chatHtml += `
                    //                <div id="draggable-` + value.id + `" class="ui-widget-content draggable">
                    //                 <p>` + data.user_count[value.chat_id] + `</p>
                    //         <header style="margin-top: 0px;border-bottom: 1px solid;width: 93%;margin-left: 9px;">
                    //             <div class="row">

                    //                     <div class="col-md-3">
                    //                         <span class="dot">` + value.icon + `</span>
                    //                     </div>
                    //                     <div class="col-md-6" style="margin-left: 70px;">
                    //                         <h5>` + value.name + `</h5>
                    //                     </div>`;
                        //         if (value.admin == 1) {
                        //             chatHtml += `
                    //                     <div><button data-id="` + value.cid + `" onclick="showModal();adduser(this)"><i class="fa fa-user"></i></button></div>
                    //                     </div>`;
                        //         }
                        //         chatHtml += `



                    //         </header>
                    //         <div class="chat-content" >
                    //             <ul id='content_` + value.chat_id + `' class="char">

                    //             </ul>
                    //         </div>

                    //         <footer style="border-top: 2px solid;width: 93%;margin-left: 9px;">
                    //             <span id='` + value.chat_id + `_type'>  </span>
                    //             <div class="row">
                    //                 <div class="col-md-9">`;
                        //         if (value.user_block == 1) {
                        //             chatHtml += `
                    //             <input type="text" name="chatInput"  id="` + key +
                        //                 `_chatInput"  onkeyup="return key_press(event , '` + key +
                        //                 `_chatInput' , ` + value
                        //                 .chat_id + ` , ` + value.user_id +
                        //                 ` )" class="send_key" contenteditable="" readonly>`;
                        //         } else {
                        //             chatHtml += `
                    //             <input type="text" name="chatInput"  id="` + key +
                        //                 `_chatInput"  onkeyup="return key_press(event , '` + key +
                        //                 `_chatInput' , ` + value
                        //                 .chat_id + ` , ` + value.user_id +
                        //                 ` )" class="send_key" contenteditable="" >`;
                        //         }

                        //         chatHtml += `
                    //                 </div>
                    //                 <div class="col-md-3">`;
                        //         if (value.user_block == 1) {
                        //             chatHtml += `
                    //             <button type="button" class="send"  onclick="send('` + key + `_chatInput' , ` + value
                        //                 .chat_id + ` , ` + value.user_id +
                        //                 ` );" disabled>Send</button>`;
                        //         } else {
                        //             chatHtml += `
                    //             <button type="button" class="send"  onclick="send('` + key + `_chatInput' , ` + value
                        //                 .chat_id + ` , ` + value.user_id +
                        //                 ` );" >Send</button>`;
                        //         }
                        //         chatHtml += `

                    //                 </div>
                    //             </div>
                    //         </footer>



                    // </div>`;
                        chatHtml += `

            <div class="col-sm-4 draggable" id="draggable_` + value.chat_id + `">
                <div class="card" >
                    <div class="card-header" style="background-color: whitesmoke;">
                        <div class="row">
                            <div class="col-md-4 ">
                                <span class="dot rounded-circle">
                                    <h6 style="margin-top:6%"  data-title="` + data.user_count[value
                            .chat_id] + `">` + value.icon + `</h6>
                                </span>
                            </div>
                            <div class="col-md-5">
                                <h5 style="position: absolute;left: 63px;" >` + value.name + `</h5>
                            </div>`;
                        if (value.admin == 1) {
                            chatHtml += `
                            <div class="col-md-3">
                                <a href="javascript:void(0)" data-id="` + value.cid + `" onclick="showModal('edit');adduser(this)"><i class="fa fa-user" aria-hidden="true"></i></a>
                            </div>`;
                        }
                        chatHtml += `
                        </div>

                    </div>
                    <div class="card-body">
                        <ul id='content_` + value.chat_id + `' class="char">

                        </ul>
                    </div>
                    <div class="card-footer" style="background-color: whitesmoke;">
                        <span id='` + value.chat_id + `_type'>  </span>
                        `;
                        if (value.user_block == 1) {
                            chatHtml += `
                            <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="chatInput"  id="` + key +
                                `_chatInput"  onkeyup="return key_press(event , '` + key +
                                `_chatInput' , ` + value
                                .chat_id + ` , ` + value.user_id +
                                ` )" class="send_key form-control" contenteditable="" readonly >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="send('` + key +
                                `_chatInput' , ` + value
                                .chat_id + ` , ` + value.user_id +
                                ` );" disabled><i class="fa fa-send"></i></a>
                            </div>
                            </div>
                            `;
                        } else {
                            chatHtml += `
                            <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="chatInput"  id="` + key +
                                `_chatInput"  onkeyup="return key_press(event , '` + key +
                                `_chatInput' , ` + value
                                .chat_id + ` , ` + value.user_id +
                                ` )" class="send_key form-control" contenteditable=""  >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="send('` + key +
                                `_chatInput' , ` + value
                                .chat_id + ` , ` + value.user_id +
                                ` );" ><i class="fa fa-send"></i></a>
                            </div>
                             </div>
                            `;
                        }
                        chatHtml += `




                    </div>
                </div>
            </div>

       `;
                        $('.move_chat').html(chatHtml);

                        my_chats(value.chat_id)


                        $(".draggable").draggable({
                            containment: '#content',
                            cursor: 'move',
                            snap: '#content'
                        });
                    })
                    chatHtml += `
                  </div>`;
                }

            })

            function my_chats(chat_id) {

                $.ajax({
                    type: "POST",
                    url: "/get_move_data",
                    data: {
                        "chat_id": chat_id
                    },
                    success: function(chat) {

                        $("#content_" + chat_id).html(chat)
                    }
                })

            }
            // $("#draggable").draggable({
            //     containment: '#content',
            //     cursor: 'move',
            //     snap: '#content'
            // });

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#quickForm').on('click', function(e) {
            e.preventDefault();
            $('#scroll_data_store').html('')
            // var form = new FormData(this);
            var name = $('#name').val();
            var icon = $('#icon').val();
            // var user_id = $('#user_id').val();
            var user_arr = new Array();
            $("input[name='user_id[]']:checked").each(function() {
                user_arr.push($(this).val());
            });

            $.ajax({
                url: "/create",
                type: "POST",
                data: {
                    'name': name,
                    'icon': icon,
                    'user_id': user_arr,
                },
                // processData: false,
                // contentType: false,
                success: function(data) {
                    // console.log(data);
                    // return false
                    $(data).each(function(key, value) {
                        if (value.success) {
                            $('#error').html('');
                            $('#error').hide();
                            $('#success').show();
                            $('#success').append(value.success);
                            $('#name').val('');
                            $('#icon').val('');
                            // $('#quickForm').trigger('reset');
                            // $('#quickForm').scrollTop($("#success"));
                        } else {

                            $('#success').hide();
                            $('#error').show();
                            $('#error').html('');
                            $('#success').html('');
                            var error = data.error.toString().replaceAll(',',
                                '<br/>');
                            $('#error').html(error);
                            $('#messageChat').scrollTop($("#error"));
                        }
                    })
                }
            })
        })



        function change(att) {
            // console.log($(att));
            $('input[type=checkbox]').removeAttr('checked')
            if ($('.sel_' + att).hasClass('this')) {
                $('.sel_' + att).removeClass('this')
            } else {
                $('.sel_' + att).addClass('this')
            }
            $('.this').attr('checked', 'checked')

            var name = $('.show_check_name')
            name.html('')

            var user_l = $('#scroll_data_store .this').length
            if (user_l >= 1) {
                name.append('<li>You & <input type="checkbox" checked hidden name="user_id[]" value="' +
                    {{ App\Models\User::get_current_user_id() }} + '" /> </li>')
            }
            $.each($('#scroll_data_store .this'), function(k, v) {
                name.append('<li>' + $(v).parent('div').children('label').html() + '</li>')
            })
            // console.log(user_l)
            // $(att).prop('checked',true)
        }

        $('#search').on('keyup', function() {

            var l = $(this).val().toLowerCase()
            if (l != '') {
                $('#scroll_data').html('')
                var s = $("#scroll_data_store label")

                $.each(s, function(key, val) {
                    if ($(val).text().toLowerCase().includes(l)) {
                        var id = $(val).attr('for')
                        var t = $("#scroll_data").append($("input[id=" + id + "]").parent('div').parent(
                            'div').html())
                        // console.log($("input[id=" + id + "]").parent('div').html())
                    }

                })

            } else {

                $('#scroll_data').html($("#scroll_data_store").html())
            }

        });
        search();

        function search() {
            var keyword = $('#search').val();
            $.post("{{ route('message.search') }}", {
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    keyword: keyword
                },
                function(data) {
                    table_post_row(data);
                    // console.log(data);
                });
        }
        // table row with ajax
        function table_post_row(res) {
            // console.log(res);
            let htmlView = '';
            if (res.search_user_data.length <= 0) {
                htmlView += `
                    <div class="form-check form-check-info text-left">
                        <label class="form-check-label">
                            No Record
                            </label>
                        </div>`;
            }
            for (let i = 0; i < res.search_user_data.length; i++) {
                htmlView += `
        <div>
            <div class="form-check form-check-info text-left">
                <input class="form-check-input sel_` + res.search_user_data[i].id + `" onchange='change(` + res
                    .search_user_data[i].id + `)' type="checkbox"
                    value="` + res.search_user_data[i].id + `" name="user_id[]"
                    id="` + res.search_user_data[i].id + `">
                <label class="form-check-label"
                    for="` + res.search_user_data[i].id + `">
                        ` + res.search_user_data[i].first_name + ' ' + res.search_user_data[i].last_name + `
                </label>
            </div>
        </div>
           `;
            }

            $('#scroll_data').html(htmlView);
            $('#scroll_data_store').html(htmlView);
            store = htmlView
            // console.log(store)
        }



        // $('.send').on('click',function (e) {
        function send(text_val, chat_id, sender) {
            // console.log(' --- ', text_val)
            // e.preventDefault();
            var text_val1 = $('#' + text_val);
            text_val = text_val1.val();

            // console.log(text_val+ ' -- ' + chat_id + ' -- ' + sender );
            // let a = [text_val, sender, chat_id];
            $.ajax({
                url: "sendmessage",
                type: 'POST',
                data: {
                    'text_val': text_val,
                    'chat_id': chat_id,
                    'sender': sender,
                },
                // dataType : 'json',
                // processData: false,
                // contentType: false,
                success: function(data) {
                    if (data.success == true) {
                        console.log(sender);
                        data = Array(text_val, chat_id, sender, 'send')
                        socket.emit('sendChatToServer', data);
                        append_chat(text_val, chat_id, sender, 'send')
                        text_val1.val('');
                        $('#' + chat_id + '_type').html('')

                    } else {
                        alert('no masg')
                    }
                    // console.log(data);
                }
            })
            // let send = socket.emit('sendChatToServer', a);
        }

        function append_chat(text_val, chat_id, sender, type) {
            // console.log(type)
            if (type == 'type') {
                $('#' + chat_id + '_type').html('typing...')
            } else if (type == 'no') {
                $('#' + chat_id + '_type').html('')
            } else if (type == 'send') {
                $('#content_' + chat_id).append(`<li value="${text_val}">${text_val}</li>`)
                $('#' + chat_id + '_type').html('');
                // $('#content_'+chat_id).slideDown(text_val);
                var ht = 0;
                $('#content_' + chat_id + ' li').each(function() {
                    // console.log(this);
                    ht += $(this).height();
                })
                // console.log(ht.toFixed());
                var convertType = ht.toFixed();
                $('#content_' + chat_id).animate({
                    scrollTop: convertType
                });
                // var scrollHeight = $('#content_'+chat_id+' li').height();
                // $('#content_'+chat_id).animate({scrollTop : scrollHeight});
            }

        }

        function adduser(val) {
            // $('.name_div').hide();
            // $('.icon_div').hide();
            // $('#name').val('');
            // $('#icon').val('');
            // // console.log(val);
            // $('.chat_label').html('');
            // $('.chat_label').append('<i class="fa fa-user">Add User </i>');
            var room_id = $(val).attr('data-id');
            $('#chat_hidden').val(room_id)
            $('#adduser').show();
            $('#quickForm').hide();
            $.ajax({
                url: "/check_room",
                type: "GET",
                data: {
                    'room_id': room_id,
                },
                success: function(data) {

                    // console.log(data);
                    var arr = [];
                    $('input[name="user_id[]"]').each(function() {
                        arr.push($(this).val());
                        // if (value.user_id == $(v).attr('id')) {
                        //     $('input[name="user_id[]"][id="' + $(v).attr('id') + '"]').attr(
                        //         'checked', 'checked')
                        // }
                    })
                    // console.log(arr);
                    // var parent = [];
                    $(data.room).each(function(key, value) {
                        // console.log(arr);
                        // console.log(value);
                        // $('input[name="user_id[]"]').each(function(k, v) {
                        if ($('.sel_' + value.user_id).hasClass('this')) {
                            $('.sel_' + value.user_id).removeClass('this')
                        } else {
                            $('.sel_' + value.user_id).addClass('this')
                        }
                        if (jQuery.inArray(value.user_id, arr)) {
                            // console.log('yes');
                            if (value.user_block == 0) {
                                $('input[name="user_id[]"][id="' + value.user_id + '"]').attr(
                                    'checked', 'checked')
                            }

                        }

                        var name = $('.show_check_name')
                        name.html('')

                        var user_l = $('#scroll_data_store .this').length
                        if (user_l >= 1) {
                            name.append(
                                '<li>You & <input type="checkbox" checked hidden name="user_id[]" value="' +
                                {{ App\Models\User::get_current_user_id() }} +
                                '" /> </li>')
                        }
                        $.each($('#scroll_data_store .this'), function(k, v) {
                            // console.log(v.checked);
                            if (v.checked == true) {
                                name.append('<li>' + $(v).parent('div').children(
                                        'label')
                                    .html() +
                                    '</li>')
                            }

                        })
                        // else{
                        //     console.log('No');
                        // }
                        // })
                        // var parent_user = $('input[name="user_id[]"]').val();
                        // console.log(parent.push(parent_user));
                        // console.log(parent_user);
                        // if (value.user_id == parent_user) {

                        //     $('input[name="user_id[]"][id="'+parent_user+'"]').attr('checked', 'checked')
                        // }
                    })
                }
            })
        }

        // $('#adduser').click(function(e){
        //     e.preventDefault();
        //     // alert('yes')
        //     var chat_hidden_id = $('#chat_hidden').val();
        //     // console.log(chat_hidden_id);
        //     var user_arr = new Array();
        //     $("input[name='user_id[]']:checked").each(function() {
        //         user_arr.push($(this).val());
        //     });

        //     console.log(user_arr);
        // })

        $('#adduser').click(function(e) {
            e.preventDefault();
            // alert('yes')
            var chat_hidden_id = $('#chat_hidden').val();
            // console.log(chat_hidden_id);
            var user_arr = [];
            $("input[name='user_id[]']").each(function(key, val) {
                // console.log(val.value);
                var state = $('input[name="user_id[]"][value=' + val.value + ']').attr('checked');
                // console.log(state);
                if (state == 'checked') {
                    user_arr.push({
                        user_id: val.value,
                        condition: 'checked'
                    });
                } else {
                    user_arr.push({
                        user_id: val.value,
                        condition: 'unchecked'
                    });
                }
                // console.log(val);
                // if ($('.sel_' + val.value).hasClass('this')) {
                //     user_arr.push({
                //         user_id: val.value,
                //         condition: 'checked'
                //     });
                //     // console.log('check');
                // } else {
                //     user_arr.push({
                //         user_id: val.value,
                //         condition: 'unchecked'
                //     });
                //     // console.log('Unchecked');
                // }
                // user_arr.push($(this).val());
            });
            const uniqueArray = user_arr.filter((v, i, a) => a.findIndex(t => (t.user_id === v.user_id)) === i)
            // console.log(uniqueArray);

            $.ajax({
                url: '/adduser',
                type: 'POST',
                data: {
                    'chat_id': chat_hidden_id,
                    'user_arr': uniqueArray,
                },
                success: function(data) {
                    // console.log(data);
                    alert(data.message);
                }
            })

        })

        $('#searchuser').click(function(e) {
            e.preventDefault();
            // alert('alert');
            var user_arr = [];
            // $("input[name='user_id[]']").each(function(key, val) {
            //     console.log(val.value);
            //     // var state = $('input[name="user_id[]"][value=' + val.value + ']').attr('checked');
            //     // console.log(state);
            // });
            $("input[class^='form-check-input'][name='user_id[]']:checked").each(function() {
                user_arr.push($(this).val());
            });
            // console.log(user_arr);
            // const uniqueArray = user_arr.filter((v, i, a) => a.findIndex(t => (t.user_id == v.user_id)) == i)
            const uniqueArray = user_arr.filter(function(element, index, self) {
                return index === self.indexOf(element);
            });
            // console.log(uniqueArray);
            $.ajax({
                url: "/search_user",
                type: "GET",
                data: {

                    'array': uniqueArray,
                },
                success: function(data) {
                    // console.log(data);
                    $(data).each(function(k, val) {
                        // console.log('Chat Id' + value.chat_id);
                        $('.draggable').each(function(key, value) {
                            if ('draggable_' + val.chat_id == value.id) {
                                // console.log('Yes');

                            } else {
                                // console.log('No');
                            }
                            // console.log('draggable_' + val.chat_id);
                            // console.log('Drop Value' + value.id)
                        })

                    })
                }
            })
            // console.log(uniqueArray);




        });

        // let message = $(this).val();
        // console.log(message);
        //         // if (e.which === 13 && !e.shiftKey && message != '') {
        //         //     let cur_user = {{ App\Models\User::get_current_user_id() }}
        //         //     let chat_id = 12;
        //             let a = [message, cur_user, chat_id];

        //             let send = socket.emit('sendChatToServer', a);
        //     if (send.connected) {}
        //     $('#chat-' + chat_id + ' ul').append(`<li>${message}</li>`)
        //     return false;
        // }
    </script>
@endsection
