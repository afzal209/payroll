<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_details')->insert([
            [
                'user_id' => '1',
                'emp_id' => '01',
                'first_name' => 'Super',
                'last_name' => 'Admin',
                'date_of_brith' => '09/06/1994',
                'date_of_joining' => '01/12/21',
                'nic' => '4230122944705',
                'next_of_kin' => 'Father',
                'phone_number' => '03242020457',
            ]
        ]);
    }
}
