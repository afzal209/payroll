<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-permission',
            'user-delete',
            'pay-list',
            'pay-create',
            'pay-edit',
            'pay-delete',
            'setting-list',
            'setting-create',
            'setting-edit',
            'role-list',
            'role-create',
            'role-edit',


        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
