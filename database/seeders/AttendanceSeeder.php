<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AttendanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('user_attendance_statuses')->insert([]);
    }
}