<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PayrollController;
use App\Http\Controllers\AppSettingController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\chatController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('user.index');
// });


// Route::group(['middleware' => 'prevent-back-history'], function () {
// login Route
Auth::routes();
// End login Route
Route::group(['middleware' => ['auth', 'cors']], function () {
    Route::get('/', [DashboardController::class, 'dashboard']);

    Route::get('/user', [UserController::class, 'index']);

    Route::post('/insert', [UserController::class, 'insert']);

    Route::post('/user_detail', [UserController::class, 'user_detail']);

    Route::get('/getUserdt/{user_id}', [UserController::class, 'get_user_data']);

    Route::get('/more_permission/{id}', [UserController::class, 'more_permission']);

    Route::get('/view', [UserController::class, 'view']);

    Route::get('/view_data', [UserController::class, 'view_data']);

    Route::get('/edit/{id}', [UserController::class, 'edit']);

    Route::post('/update/{id}', [UserController::class, 'update']);

    Route::post('/updatedetail/{id}', [UserController::class, 'update_detail']);

    Route::post('/insert_more_permission/{id}', [UserController::class, 'insert_more_permission']);

    Route::delete('/delete/{id}', [UserController::class, 'delete']);

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    //  End user route

    // Payroll Route

    Route::get('/payroll', [PayrollController::class, 'index']);

    Route::post('/insert_payroll', [PayrollController::class, 'insert_payroll']);


    //  End Payroll Route

    //  App Setting

    Route::get('/app_setting', [AppSettingController::class, 'index']);

    Route::get('/app_setting_data', [AppSettingController::class, 'index_data']);

    Route::get('/add_page_setting', [AppSettingController::class, 'insert_page']);

    Route::post('/insert_setting', [AppSettingController::class, 'insert']);

    Route::get('/edit_setting/{id}', [AppSettingController::class, 'edit_setting']);

    Route::post('/update_setting/{id}', [AppSettingController::class, 'update_setting']);
    // End App Setting

    // Role

    Route::get('/role', [RoleController::class, 'index'])->name('role');

    Route::get('/role_data', [RoleController::class, 'index_data']);

    Route::get('/add_page', [RoleController::class, 'insert_role'])->name('add_page');

    Route::post('/insert_role', [RoleController::class, 'insert'])->name('insert_role');

    Route::get('/edit_role/{id}', [RoleController::class, 'edit_role'])->name('edit_role');

    Route::post('/update_role/{id}', [RoleController::class, 'update_role'])->name('update_role');
    Route::get('/message', [chatController::class, 'index']);

    Route::post('/create', [chatController::class, 'create']);

    Route::post('/search_user_data/search', [chatController::class, 'search_user_data'])->name('message.search');

    Route::get('/move_chat', [chatController::class, 'move_chat']);

    Route::post('/get_move_data', [chatController::class, 'get_move_data']);

    Route::get('/check_room', [chatController::class, 'check_room']);
    // Route::get('/search_user_data/{search}',[chatController::class,'search_user_data']);
    Route::post('sendmessage', [chatController::class, 'sendMessage']);

    Route::post('/adduser', [chatController::class, 'adduser']);

    Route::get('/search_user', [chatController::class, 'search_user']);
});
    // user route



// });