<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use function PHPUnit\Framework\isEmpty;

class app_setting extends Model
{
    use HasFactory, SoftDeletes;



    public static function name($name)
    {
        $name = app_setting::where('code', $name)->first();
        if ($name == '') {
            return 'No Setting';
        } else {
            return  $name->value;
        }
    }

    // public static function logo()
    // {
    //     return 'logo';
    // }
}