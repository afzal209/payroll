<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_role extends Model
{
    use HasFactory;

    const ADMIN = 'Admin';
    const HR = 'HR';

    public static function admin()
    {
        return user_role::where('role_name', self::ADMIN);
    }
    public static function hr()
    {
        return user_role::where('role_name', self::HR);
    }
}