<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Models\chat;
use App\Models\chat_users;
use App\Models\chat_msg;
use App\Models\User;
use App\Models\user_detail;
use Illuminate\Support\Facades\Auth;
use DB;

class chatController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $user = User::all();
        $chat_data = DB::table('chats')
            ->join('chat_users', 'chats.id', '=', 'chat_users.chat_id')
            ->where('chat_users.user_id', User::get_current_user_id())
            ->select('chats.id  as cid', 'chats.name', 'chats.icon', 'chats.deleted_at', 'chat_users.*')
            ->get();
        // dd(User::get_current_user_id());
        // return view('message.index',compact('user'));

        return view('message.index');
    }


    public function move_chat()
    {
        $chat_count = [];
        $chat_datas = DB::table('chats')
            ->join('chat_users', 'chats.id', '=', 'chat_users.chat_id')
            ->join('users', 'users.id', '=', 'chat_users.user_id')
            ->where('chat_users.user_id', User::get_current_user_id())
            ->select('chats.id  as cid', 'chats.name', 'chats.icon', 'chats.deleted_at', 'chat_users.*', 'users.name as user_name')
            ->get();
        foreach ($chat_datas as $key => $value) {

            $chat_count[$value->chat_id] = DB::table('chat_users')->where('chat_id', $value->chat_id)->count();
        }
        return response()->json(['data' => $chat_datas, 'user_count' => $chat_count]);
    }

    public function create(Request $request)
    {
        // return $request;
        $validator = \Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'name' => 'required',
                'icon' => 'required',
            ],
            [
                'user_id.required' => 'User Required',
                'name.required' => 'Group  Name Required',
                'icon.required' => 'Icon Required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $chat = new chat();
        $chat->name = $request->name;
        $chat->icon = $request->icon;
        $chat->save();

        foreach ($request->user_id as $user) {
            $chat_users = new chat_users();
            $chat_users->chat_id = $chat->id;
            $chat_users->user_id = $user;
            $chat_users->user_block = 0;
            $chat_users->admin = ($user == User::get_current_user_id()) ? 1 : 0;
            $chat_users->save();
        }

        return response()->json(['success' => 'Chat Room has been Created']);
    }

    public function search_user_data(Request $request)
    {
        // return $search;
        // $search_user_data = '';
        // if($search != ''){
        //     $search_user_data = User::where('name',$search)->get();
        // }
        // else{
        //     $search_user_data = User::all();
        // }
        // return response()->json(['data' => $search_user_data]);
        // $search_user_data = User::where('id','!=',Auth::user()->id)->get();
        $search_user_data = user_detail::where('user_id', '!=', User::get_current_user_id())->get();

        if ($request->keyword != '') {
            $search_user_data = user_detail::select('*')->where('first_name', 'LIKE', '%' . $request->keyword . '%')
                ->where('last_name', 'LIKE', '%' . $request->keyword . '%')
                ->where('user_id', '!=', User::get_current_user_id())->get();
        }
        return response()->json([
            'search_user_data' => $search_user_data
        ]);
    }

    public function get_move_data(Request $request)
    {

        // $nestedArray = array();
        // $method = '@method';
        $nestedArray1 = '';
        $chat_id = $request->chat_id;
        $show_chat = chat_msg::where('chat_id', $chat_id)
            ->orderBy('id', 'desc')->get();
        if ($show_chat->isEmpty()) {
            $nestedArray1 .= '<li class="chat_align">No Chat</li>';
            // return 'Null';
        } else {
            // return 'Not Null';

            foreach ($show_chat as $val) {

                $nestedArray1 .= '<li value="' . $val->msg . '" >' . $val->msg . '</i>';

                // $nestedArray[] = $nestedArray1;
            }
        }



        // $data = [
        //     'data' => $nestedArray1
        // ];
        return $nestedArray1;
    }

    public function check_room(Request $request)
    {
        // return $request;
        $room = DB::table('chats')
            ->join('chat_users', 'chats.id', '=', 'chat_users.chat_id')
            ->where('chat_users.chat_id', $request->room_id)

            ->select('chat_users.user_id', 'chat_users.admin', 'chat_users.user_block')
            ->get();
        // return $room;
        return response()->json(['room' => $room]);
    }
    public function sendMessage(Request $request)
    {
        // return $request;
        $chat_msg = new chat_msg();
        $chat_msg->chat_id = $request->chat_id;
        $chat_msg->msg = $request->text_val;
        $chat_msg->msg_user = $request->sender;
        $chat_msg->save();
        return response()->json(['success' => true]);
        // return $request;
        // return $request;
        // $redis = Redis::connection();
        // // // return $redis;
        // $data = ['message' => $request->message, 'user' => $request->user];

        // $redis->publish('message', json_encode($data));

        // return response()->json(['success' => true]);
    }
    public function adduser(Request $request)
    {
        $user_array = [];
        // return $request->user_arr;
        foreach ($request->user_arr as $user) {
            // print_r($user['user_id']);
            $user_array[] = $user;
        }
        // return $user_array;
        for ($i = 0; $i < count($user_array); $i++) {
            $user_chat_id = chat_users::where('user_id', $user_array[$i]['user_id'])->where('chat_id', $request->chat_id)->get();
            if ($user_array[$i]['condition'] == 'checked') {
                // print_r($user_array[$i]['condition']);
                $convert_array = json_decode(json_encode($user_chat_id));
                // print_r($convert_array);
                if (!empty($convert_array)) {
                    chat_users::where('user_id', $user_array[$i]['user_id'])->where('chat_id', $request->chat_id)->update([
                        'user_block' => 0,
                    ]);
                } else {
                    $user_chat_insert = new chat_users();
                    $user_chat_insert->chat_id = $request->chat_id;
                    $user_chat_insert->user_id = $user_array[$i]['user_id'];
                    $user_chat_insert->user_block = 0;
                    $user_chat_insert->admin = ($user_array[$i]['user_id'] == User::get_current_user_id()) ? 1 : 0;
                    $user_chat_insert->save();
                }
            } elseif ($user_array[$i]['condition'] == 'unchecked') {
                // print_r($user_array[$i]['condition']);
                chat_users::where('user_id', $user_array[$i]['user_id'])->where('chat_id', $request->chat_id)->update([
                    'user_block' => 1,
                ]);
            }
        }
        // return "";
        return response()->json(['message' => 'Update']);
    }

    public function search_user(Request $request)
    {

        // print_r(implode(",", $request->array));
        $imp = implode(",", $request->array);

        // return $request;
        // $array_user = [];
        // foreach ($request->array as $req_arr) {
        // print_r($req_arr);
        // $chat_user_search = chat_users::where('user_id', $req_arr)->get();

        $chat_user_search = DB::table('chat_users as cu')
            ->join('chat_users as cu1', 'cu1.chat_id', '=', 'cu.chat_id')
            ->select('cu.chat_id')
            ->where('cu.user_id', $imp)
            ->where('cu1.user_id', User::get_current_user_id())
            ->get();
        // // $array_user[] = $chat_user_search;
        // // print_r($chat_user_search);
        // $array_user[] = $chat_user_search;
        // }

        return $chat_user_search;
    }
}