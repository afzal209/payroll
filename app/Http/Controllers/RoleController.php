<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
class RoleController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('permission:role-create', ['only' => ['insert_role', 'insert']]);
        $this->middleware('permission:role-list', ['only' => ['index']]);
    }

    public function index(){
        $role = Role::all();
        return view('role.index');
    }

    public function index_data(){
        $role = Role::select('id','name')->get();

        $nestedArray= array();
        // $method = '@method';
        $i = '';
        foreach($role as $val){


            $btn  = '<a href="/edit_role/'.base64_encode($val->id).'"><i class="fas fa-edit"></i></a>';


            $nestedArray1= array();

            $nestedArray1[] = ++$i;
            $nestedArray1[] = $val->name;
            $nestedArray1[] = $btn;

            $nestedArray[] = $nestedArray1;

        }


        $data = [
            'data'=>$nestedArray
        ];
        return response()->json($data);
    }
    public function insert_role(){
        $permission = Permission::get();
        return view('role.insert_page',compact('permission'));
    }

    public function insert(Request $request){
        // return $request;
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ],
        [
           'name.required' => 'Role Name Required',
           'name.unique' => 'Role Name Must Be Unique',
           'permission.required' => 'Permission Required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));

        // return redirect()->route('role')
        //                 ->with('message','Role created successfully');

        return response()->json(['success' => 'Role created successfully']);
    }

    public function edit_role($id){
        $id = base64_decode($id);
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id')
            ->all();
        //  return $rolePermissions;
        return view('role.edit',compact('role','permission','rolePermissions'));
    }

    public function update_role(Request $request ,$id){

        // return $request;
        $id = base64_decode($request->id);
        $id = str_replace('|i','',$id);
        // return $id;
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'permission' => 'required',
        ],
        [
            'name.required' => 'Name Required',
            'permission.required' => 'Permission Required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return response()->json(['success'=>'Role updated successfully']);
        // return redirect()->route('role')
        //                 ->with('message','Role updated successfully');
    }
}
