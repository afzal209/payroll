<?php

namespace App\Http\Controllers;

use App\Models\app_setting;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Auth;

class AppSettingController extends Controller
{
    //
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('permission:setting-list', ['only' => ['index']]);
        $this->middleware('permission:setting-create', ['only' => ['insert_page','insert']]);
        $this->middleware('permission:setting-edit',['only' => ['edit_setting','update_setting']]);
    }

    public function index()
    {
        // if(Auth::user()->hasPermissionTo('setting-list')){


            return view('app_setting.index');
        // }

    }

    public function index_data(){
        $app_setting = app_setting::select('id','code','value','description')->get();
            // return $app_setting;
            $nestedArray= array();
            // $method = '@method';

            foreach($app_setting as $val){


                $btn  = '<a href="/edit_setting/'.base64_encode($val->id).'"><i class="fas fa-edit"></i></a>';


                $nestedArray1= array();

                $nestedArray1[] = $val->code;
                $nestedArray1[] = $val->value;
                $nestedArray1[] = $val->description;
                $nestedArray1[] = $btn;

                $nestedArray[] = $nestedArray1;

            }


            $data = [
                'data'=>$nestedArray
            ];
            return response()->json($data);
    }

    public function insert_page()
    {

        return view('app_setting.insert_page');
    }

    public function insert(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'code' => 'required|unique:app_settings,code',
            'value' => 'required',
            'description' => 'required',
        ], [
            'code.required' => 'Code Required',
            'code.unique' => 'Code Must be unique',
            'value' => 'Value Required',
            'description' => 'Description Required',
        ]);


        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }

        $app_setting = new app_setting();
        $app_setting->code = $request->code;

        $app_setting->value = $request->value;
        $app_setting->description = $request->description;
        $app_setting->save();

        // return redirect('/add_page')->with('message', 'Setting has been add');
        return response()->json(['success' => 'Setting has been add']);
    }

    public function edit_setting($id)
    {
        $id = base64_decode($id);
        $app_setting = app_setting::find($id);
        return view('app_setting.edit')->with('app_setting', $app_setting);
    }

    public function update_setting(Request $request, $id)
    {
        $id = base64_decode($request->id);
        $id = str_replace('|i','',$id);
        // return $id;
        // return $id;

        $app_setting = app_setting::find($id);
        $app_setting->code = $request->code;
        $app_setting->value = $request->value;
        $app_setting->description = $request->description;
        $app_setting->save();

        return response()->json(['success' => 'Setting has been updated']);
        // return redirect('/app_setting')->with('message', 'Setting has been updated');
    }
}
