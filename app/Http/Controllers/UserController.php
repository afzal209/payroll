<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\user_detail;
use App\Models\user_role;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Arr;

use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('permission:user-create', ['only' => ['index', 'insert', 'user_detail', 'get_user_data']]);
        $this->middleware('permission:user-list', ['only' => ['view']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update', 'update_detail']]);
        $this->middleware('permission:user-permission', ['only' => ['more_permission', 'insert_more_permission']]);
        $this->middleware('permission:user-delete', ['only' => ['delete']]);
    }



    public function index()
    {
        $users = User::all();
        $user_role = Role::all();
        return view('user.index', compact('user_role', 'users'));
    }

    public function insert(Request $request)
    {

        // return $request->age;
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'emp_id' => 'required',
            //     'first_name' => 'required|unique:user_details,first_name',
            //     'last_name' => 'required|unique:user_details,last_name',
            //     'date_of_brith' => 'required',
            //     'date_of_joining' => 'required',
            //     'nic' => 'required',
            //     'next_of_kin' => 'required',
            //     'phone_number' => 'required',
            //     'role_id' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
        ], [
            'name.required' => 'Username Required',
            'emp_id.required' => 'Employee Id Required',
            //     'first_name.required' => 'First Name Required',
            //     'first_name.unique' => 'First Name Already taken',
            //     'last_name.required' => 'Last Name Required',
            //     'last_name.unique' => 'Last Name Already taken',
            //     'date_of_brith.required' => 'Date of Brith Required',
            //     'date_of_joining.required' => 'Date of Join Reqired',
            //     'nic.required' => 'National Identity Card Required',
            //     'next_of_kin.required' => 'Next of Kin Required',
            //     'phone_number.required' => 'Phone Number Required',
            //     'role_id.required' => 'Role Required',
            'email.required' => 'Email Required',
            //     'email.unique' => 'Email Already Taken',
            'password.required' => 'Password is Required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $user = new User();
        $ar = array('email' => $request->email, 'deleted_at' => null);
        $find = User::where($ar)->get();

        if (count($find) > 0) {
            return response()->json(['error' => 'Email Already Exist']);
        }
        // return $find;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->assignRole($request->role_id);
        $user->save();

        $userDetail = new user_detail();

        $userDetail->user_id = $user->id;
        $userDetail->emp_id = $request->emp_id;
        $userDetail->save();

        return response()->json(['success' => 'User Added']);
        // return redirect('/')->with('add_message', 'User Add');
    }

    public function user_detail(Request $request)
    {
        // return $request;
        // die();
        $validator = \Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
            ],
            [
                'user_id.required' => 'Username in Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }
        $user_detail = DB::table('user_details')->where('user_id', $request->user_id)->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'date_of_brith' => $request->date_of_brith,
            'date_of_joining' => $request->date_of_joining,
            'nic' => $request->nic,
            'next_of_kin' => $request->next_of_kin,
            'phone_number' => $request->phone_number
        ]);
        // return redirect('/')->with('message', 'User Detail Added');
        return response()->json(['success' => 'User Detail Added']);
        // return $user_detail;

        // $user_detail->first_name = $request->first_name;
        // $user_detail->last_name = $request->last_name;
        // $user_detail->date_of_brith  = $request->date_of_brith;
        // $user_detail->date_of_joining = $request->date_of_joining;
        // $user_detail->nic = $request->nic;
        // $user_detail->next_of_kin = $request->next_of_kin;
        // $user_detail->phone_number = $request->phone_number;
        // $user_detail->save();

    }

    public function get_user_data($id)
    {
        // return $id;
        $user_detail = DB::table('user_details')->where('user_id', $id)->get();
        return $user_detail;
    }
    public function view()
    {
        // $user = DB::table('users')
        //     ->join('user_details', 'users.id', '=', 'user_details.user_id')
        //     ->select('users.*', 'user_details.first_name', 'last_name', 'user_details.id as user_detail_id')
        //     ->where('users.deleted_at', '=', null)
        //     // ->where('user_details.deleted_at', '=', null)
        //     ->get();

        return view('user.view');
    }

    public function view_data()
    {

        $view_user = DB::table('users')
            ->join('user_details', 'users.id', '=', 'user_details.user_id')
            ->select(
                'users.id as id',
                'users.name as Username',
                'users.email as email',
                'user_details.first_name as first_name',
                'user_details.last_name as last_name',
                'user_details.id as user_detail_id'
            )
            ->where('users.deleted_at', '=', null)
            // ->where('user_details.deleted_at', '=', null)
            ->get();

        $nestedArray = array();
        // $method = '@method';

        foreach ($view_user as $val) {


            $btn  = '
            <a href="/edit/' . base64_encode($val->id) . '"><i class="fas fa-edit"></i></a>/
            <a href="javascript:void(0)" onclick="delete_user(this)" data-id="' . base64_encode($val->id) . '"><i class="fas fa-trash"></i></a>
            <a href="/more_permission/' . base64_encode($val->id) . '"><i
            class="fa fa-lock"></i></a>';


            $nestedArray1 = array();

            $nestedArray1[] = $val->Username;
            $nestedArray1[] = $val->first_name;
            $nestedArray1[] = $val->last_name;
            $nestedArray1[] = $val->email;
            $nestedArray1[] = $btn;

            $nestedArray[] = $nestedArray1;
        }


        $data = [
            'data' => $nestedArray
        ];
        return response()->json($data);
    }

    public function edit($id)
    {
        $id = base64_decode($id);

        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        // $user_role = Role::all();
        $user_detail = DB::table('users')
            ->join('user_details', 'users.id', '=', 'user_details.user_id')
            ->select('user_details.*', 'user_details.id as user_detail_id')
            ->where('users.id', $id)
            ->first();
        // return $roles;
        return view('user.update', compact('user', 'roles', 'userRole', 'user_detail'));
    }

    public function update(Request $req, $id)
    {
        $id = base64_decode($req->id);
        $id = str_replace('|i', '', $id);

        $validator = \Validator::make($req->all(), [
            'name' => 'required',
            // 'email' => 'required',
            'password' => 'required|min:8',
        ], [
            'name.required' => 'Username Requied',
            // 'email.required' => 'Email Required',
            'password.required' => 'Password is Required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        // // return $req;

        $user = User::find($id);
        // return $user;
        $user->name = $req->name;
        // // $user->email = $req->email;
        $user->password = bcrypt($req->password);
        $user->save();
        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($req->input('role_id'));
        return response()->json(['success' => 'Data Updated']);
        // return redirect('/view')->with('message', 'Data Updated');
    }

    public function update_detail(Request $request, $id)
    {
        // return $id;
        $id = base64_decode($request->id_detail);
        $id = str_replace('|i', '', $id);
        // // return $request->id_detail;
        // return $id;
        // $user_detail = user_detail::find($id);
        $user_detail = DB::table('user_details')
            ->where('user_id', $id)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'date_of_brith' => $request->date_of_brith,
                'date_of_joining' => $request->date_of_joining,
                'nic' => $request->nic,
                'next_of_kin' => $request->next_of_kin,
                'phone_number' => $request->phone_number,
            ]);
        // return $user_detail;
        // $user_detail->first_name = $request->first_name;
        // $user_detail->last_name = $request->last_name;
        // $user_detail->date_of_brith = $request->date_of_brith;
        // $user_detail->date_of_joining = $request->date_of_joining;
        // $user_detail->nic = $request->nic;
        // $user_detail->next_of_kin = $request->next_of_kin;
        // $user_detail->phone_number = $request->phone_number;
        // $user_detail->save();
        return response()->json(['success' => 'User Data Updated']);
        // return redirect('/view')->with('message', 'Data Updated');
    }

    public function more_permission($id)
    {
        $id = base64_decode($id);
        $user = User::where('id', $id)->first();

        $permission = Permission::get();
        // return $user;

        if (DB::table("model_has_permissions")->where("model_has_permissions.model_id", $id)->get()) {
            // user permission
            $rolePermissions = DB::table("model_has_permissions")->where("model_has_permissions.model_id", $id)
                ->pluck('model_has_permissions.permission_id')
                ->all();
        } else {
            // role permission
            $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
                ->pluck('role_has_permissions.permission_id')
                ->all();
        }
        return view('user.more_permission', compact('user', 'permission', 'rolePermissions'));
    }

    public function insert_more_permission(Request $request, $id)
    {
        // return $id;
        // $role = User::create(['name' => $request->input('id')]);
        $id = base64_decode($request->id);
        $id = str_replace('|i', '', $id);
        // return $id;
        DB::table('model_has_permissions')->where('model_id', $id)->delete();

        $user = User::find($id);
        $user->givePermissionTo($request->input('permission'));
        // return 'inserted';
        return response()->json(['success' => 'Model Permission Has been insert']);
        // return redirect()->back()->with('message','Model Permission Has been insert');
        // $model_has_permission = DB::table('model_has_permissions');
        // $model_has_permission->where('model_id',$id)->delete();

        // $user = User::where('id' , $id)->get();
        // $user->syncPermissions($request->input('permission'));

        //auth()->user()->givePermissionTo($request->permission);
        // return redirect('/view')->with('message','User Has permission');
    }
    public function delete($id)
    {
        // return $id;
        $id = base64_decode($id);
        // return $id;
        $delete_user_dt = user_detail::where('user_id', $id);
        // return $delete_user_dt;
        if ($delete_user_dt->delete()) {
            $delete_user_mp = DB::table('model_has_permissions')->where('model_id', $id);
            if ($delete_user_mp->delete()) {
                $delete_user = User::find($id);
                $delete_user->delete();
            } else {
                $delete_user = User::find($id);
                $delete_user->delete();
            }
        } else {
            $delete_user = User::find($id);
            $delete_user->delete();
        }
        return response()->json(['success' => 'User Has Been Deleted']);
        // return back()->with('message', 'User Has Been Deleted');
    }



    // public function hasrole()
    // {
    //     return User::hasRole();
    // }
}