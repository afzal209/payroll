<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\user_payroll;

class PayrollController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('permission:pay-create', ['only' => ['index','insert_payroll']]);
    }

    public function index()
    {
        $users = User::all();
        return view('payoll.index', compact('users'));
    }

    public function insert_payroll(Request $request)
    {

        $validator = \Validator::make($request->all(),[
            'user_id' => 'required',
            'month_year' => 'required',
            'salary' => 'required',
        ], [
            'user_id.required' => 'User Required',
            'month_year.required' => 'Month Required',
            'salary.required' => 'Salary is Required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        // return $request;
        $user_payroll = new user_payroll();
        $user_payroll->user_id = $request->user_id;
        $user_payroll->month_year = $request->month_year;
        $user_payroll->salary = $request->salary;
        $user_payroll->save();

        // return redirect('/payroll')->with('message', 'Pay Roll has been Add');
        return response()->json(['success' => 'Pay Role has been Add']);

    }
}
